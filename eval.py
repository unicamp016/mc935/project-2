"""Evaluate a policy model (.zip) against the built-in AI."""

import argparse

import gym
import numpy as np
import slimevolleygym
from stable_baselines3.dqn import DQN
from stable_baselines3.ppo import PPO

from src.utils import get_env_name, synthesize


def rollout(env, policy, render_mode=False):
    """Play one agent vs the other in modified gym-style loop."""
    obs = env.reset()

    done, total_reward = False, 0
    while not done:
        action, _states = policy.predict(obs, deterministic=True)
        obs, reward, done, _info = env.step(action)
        total_reward += reward
        if render_mode:
            env.render()

    return total_reward


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Evaluate pre-trained agent.')
    parser.add_argument('method', choices=['DQN', 'PPO'])
    parser.add_argument(
        '--model-path',
        help='path to stable-baselines3 model.',
        type=str,
    )
    parser.add_argument(
        '--render',
        help='render to screen?',
        action='store_true',
        default=False,
    )

    args = parser.parse_args()
    render_mode = args.render

    is_DQN = args.method == 'DQN'
    env = gym.make(get_env_name(discrete_act_space=is_DQN))

    print("Loading", args.model_path)
    policy = DQN.load(args.model_path, env) if is_DQN else PPO.load(args.model_path, env)

    history = []
    for i in range(512):
        env.seed(seed=i)
        cumulative_score = rollout(env, policy, render_mode)
        print("cumulative score #", i, ":", cumulative_score)
        history.append(cumulative_score)

    print("history dump:", history)
    print("synthesis: ", synthesize(history))
