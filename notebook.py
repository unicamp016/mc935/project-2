# -*- coding: utf-8 -*-
# %%
# NOTE run this the first time:
# !wget -O requirements.txt https://pastebin.com/raw/XmwA0LeY
# !pip install -r requirements.txt
# !pip install stable-baselines3[extra]
# !pip install slimevolleygym

# %% ######################################################################### [markdown]
# # Video (Group 8): https://drive.google.com/file/d/19CxIK0_Efc96xKrzFBuzohr68Es_8IZ3/view

# %% ######################################################################### [markdown]
# # Setup

# %%
import sys

assert sys.version_info.major == 3
assert sys.version_info.minor >= 7
!python --version

# %% [markdown]
# ## Package imports and constant defines

# %%
%matplotlib inline

import inspect
import os
from contextlib import contextmanager

import gym
import matplotlib.pyplot as plt
import numpy as np
import slimevolleygym
import stable_baselines3 as sb3
import yaml
from stable_baselines3.common import logger
from stable_baselines3.common.env_checker import check_env
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.utils import set_random_seed

sb3.common.utils.get_device()  # check if you're using the CPU or GPU

# %%
MULTIBIN_ENV_NAME = "SlimeVolley-v0"
DISCRETE_ENV_NAME = "SlimeVolleyAtari-v0"

SEED = 935

EVAL_FREQ = int(5e5)
EVAL_EPISODES = 20
TOTAL_TIMESTEPS = int(45e5)
STOP_TRAIN_REWARD_THRESHOLD = 0

LOG_DIR = "logs"
TB_LOG_DIR = os.path.join(LOG_DIR, "tb_logs")

# %% [markdown]
# ## Auxiliary methods

# %%
def get_log_dir(name):
    return os.path.join(LOG_DIR, name)

def get_env_name(discrete_act_space):
    return DISCRETE_ENV_NAME if discrete_act_space else MULTIBIN_ENV_NAME

@contextmanager
def gym_env(env_name, survival_bonus=False, seed=None, **kwargs):
    """Context manager for gym environment."""
    set_random_seed(seed)
    env = gym.make(env_name, **kwargs)
    env.survival_bonus = survival_bonus  # NOTE adds +0.01 to reward from step
    env.multiagent = False  # NOTE removes otherObs from info (might be faster)
    check_env(env)
    try:
        yield env
    finally:
        env.close()

def linear_schedule(learning_rate):
    """Return a function that linearly decreases the learning rate
    with the currently remaining progress (which goes from 1 to 0).
    """
    return lambda progress: learning_rate * progress

# %%
def get_default_kwargs(func):
    """Return the default keyword arguments for `func`."""
    signature = inspect.signature(func)
    def_kwargs = {"func_name": func.__name__}
    for name, param in signature.parameters.items():
        if not param.default is param.empty:
            def_kwargs[name] = param.default
    return def_kwargs

def save_config(func):
    """Save experiment configuration."""

    def exp_wrapper(name, overwrite=False, **kwargs):
        log_dir = get_log_dir(name)
        if not overwrite and os.path.isdir(log_dir):
            print("[ERROR] Experiment directory already exists. Choose a different name.")
        os.makedirs(log_dir, exist_ok=True)
        with open(os.path.join(log_dir, "config.yaml"), "w") as f:
            def_kwargs = get_default_kwargs(func)
            def_kwargs.update(kwargs)
            yaml.dump(def_kwargs, f)
        func(name, **kwargs)

    return exp_wrapper

# %% [markdown]
# ## Environment with `Discrete` action space (instead of `MultiBinary`)

# %%
class SlimeVolleyAtariEnv(slimevolleygym.SlimeVolleyEnv):
    """SlimeVolley environment with 12-D observations and Atari actions."""
    # NOTE required by DQN
    from_pixels = False
    atari_mode = True

# %%
gym.register(
    id='SlimeVolleyAtari-v0',
    entry_point='src.slimevolley:SlimeVolleyAtariEnv')

# %% [markdown]
# ## Callbacks helpers

# %%
from stable_baselines3.common.callbacks import (
    BaseCallback,
    CallbackList,
    EvalCallback,
    StopTrainingOnRewardThreshold,
)


# %%
class TensorboardCallback(BaseCallback):
    """Custom callback for tracking Monitor data in tensorboard."""

    def __init__(self, verbose=0):
        super().__init__(verbose=verbose)
        self.elapsed_time = 0
        self.n_episodes = 0

    def _on_step(self) -> bool:
        env = self.training_env.envs[0]  # NOTE only handle the case for 1 env
        done_array = np.array(
            self.locals.get("done")
            if self.locals.get("done") is not None
            else self.locals.get("dones"))
        done_episodes = np.sum(done_array).item()
        if done_episodes:
            self.logger.record("monitor/episode_length", env.get_episode_lengths()[-1])
            self.logger.record("monitor/episode_reward", env.get_episode_rewards()[-1])
            latest_time = env.get_episode_times()[-1]
            self.logger.record("monitor/episode_time", latest_time - self.elapsed_time)
            self.elapsed_time = latest_time
            self.n_episodes += done_episodes
            self.logger.dump(step=self.n_episodes)
        return True

# %%
def get_tb_callback():
    """Get callback to log tensorboard values."""
    return TensorboardCallback(verbose=1)

def get_eval_callback(env, log_dir):
    """Return a evaluation callback with early stopping based on reward."""
    callback_on_best = StopTrainingOnRewardThreshold(
        reward_threshold=STOP_TRAIN_REWARD_THRESHOLD, verbose=1)
    return EvalCallback(
        env,
        best_model_save_path=log_dir,
        callback_on_new_best=callback_on_best,
        log_path=log_dir,
        eval_freq=EVAL_FREQ,
        n_eval_episodes=EVAL_EPISODES,
    )

def get_default_callback_list(env, log_dir):
    """Return callback with a list of default training callbacks."""
    return CallbackList([
        get_tb_callback(),
        get_eval_callback(env, log_dir),
    ])

# %% ######################################################################### [markdown]
# # Visualizations

# %%
def plot_metric(experiments, metric_name, param, xlabel, ylabel, save=None):
    with plt_fig(save, figsize=(5, 5)) as (_, ax):
        for exp in experiments:
            df = tfevents_to_dataframe(exp, [metric_name])
            t = np.array(list(df.index))
            if "eval" in metric_name:
                t *= EVAL_FREQ
            ax.plot(t, df[metric_name], label=EXP_NAME_TO_LABEL[exp][param])
        ax.legend()
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)


def plot_eval_reward(experiments, param, train=False, save=None):
    metric_type = "train" if train else "eval"
    metric_name = f"{metric_type}_ep_rew_mean"
    plot_metric(experiments, metric_name, param, "Steps", "$\\mathbb{E}[G]$", save)


def plot_eval_length(experiments, param, train=False, save=None):
    metric_type = "train" if train else "eval"
    metric_name = f"{metric_type}_ep_len_mean"
    plot_metric(experiments, metric_name, param, "Steps", "$\\mathbb{E}[\\tau]$", save)

# %% ######################################################################### [markdown]
# # Methods tested using Stable-Baselines3 ([SB3](https://stable-baselines3.readthedocs.io/en/master/))

# %% ######################################################################### [markdown]
# ## *Off-policy* algorithm: DQN

# %%
from stable_baselines3.dqn import DQN
from stable_baselines3.dqn import MlpPolicy as DqnMlpPolicy


@save_config
def dqn(
    name,
    learning_rate=1e-4,
    batch_size=64,
    survival_bonus=False,
    # NOTE using the same defaults as SB3's DQN:
    buffer_size=int(1e6),
    learning_starts=int(5e4),
    train_freq=4,
    exploration_fraction=1e-1,
    target_update_interval=int(1e4),
):
    log_dir = get_log_dir(name)
    with gym_env(get_env_name(discrete_act_space=True), survival_bonus, seed=SEED) as env:
        logger.configure(folder=log_dir)
        env = Monitor(env, log_dir)
        model = DQN(
            DqnMlpPolicy,
            env,
            batch_size=batch_size,
            learning_rate=linear_schedule(learning_rate),

            buffer_size=buffer_size,
            learning_starts=learning_starts,
            train_freq=train_freq,
            target_update_interval=target_update_interval,
            exploration_fraction=exploration_fraction,

            tensorboard_log=TB_LOG_DIR,
            verbose=2,
            seed=SEED,
        )
        callbacks = get_default_callback_list(env, log_dir)
        model.learn(total_timesteps=TOTAL_TIMESTEPS, callback=callbacks, tb_log_name=name)
        model.save(os.path.join(log_dir, "final_model_dqn"))

# %% ######################################################################### [markdown]
# ## *On-policy* algorithm: PPO

# %%
from stable_baselines3.ppo import PPO
from stable_baselines3.ppo import MlpPolicy as PpoMlpPolicy


@save_config
def ppo(
    name,
    learning_rate=3e-4,
    batch_size=64,
    survival_bonus=False,
    discrete_act_space=False,
    # NOTE using the same defaults as SB3's PPO:
    clip_range=0.2,
    target_kl=None,
    n_steps=2048,
):
    log_dir = get_log_dir(name)
    with gym_env(get_env_name(discrete_act_space), survival_bonus, seed=SEED) as env:
        logger.configure(folder=log_dir)
        env = Monitor(env, log_dir)
        model = PPO(
            PpoMlpPolicy,
            env,
            batch_size=batch_size,
            learning_rate=linear_schedule(learning_rate),

            n_steps=n_steps,
            clip_range=clip_range,
            target_kl=target_kl,

            tensorboard_log=TB_LOG_DIR,
            verbose=2,
            seed=SEED,
        )
        callbacks = get_default_callback_list(env, log_dir)
        model.learn(total_timesteps=TOTAL_TIMESTEPS, callback=callbacks, tb_log_name=name)
        model.save(os.path.join(log_dir, "final_model_ppo"))

# %% ######################################################################### [markdown]
# # Project report

# %% [markdown]
# ## Problem addressed

# %% [markdown]
# The problem we selected to work on is based on the game Slime Volleyball, created in the early 2000s by an unknown author. Similar to the game of Pong, the agent's goal is to hit the ball on the ground of its opponent's side of the field, causing it to lose a life.
#
# ![](https://camo.githubusercontent.com/e3a3e8453897b2f9f4b9fd47aa88632ac40e45a98912a9d80971d5f2b525bc3d/68747470733a2f2f6f746f726f2e6e65742f696d672f736c696d6567796d2f73746174652e676966)

# %% [markdown]
# ## Environment characteristics

# %% [markdown]
# We use an environment of this game adapted to OpenAI's Gym API, [slimevolleygym](https://github.com/hardmaru/slimevolleygym/), created by David Ha. In it, the blue agent (left side) is controlled by the game -- defined by a small (120-parameter) neural network -- and is considered as the _expert baseline_ with which we compare our agent's performance.
#
# An agent receives a reward of +1 for scoring a point and -1 for getting scored on (i.e. losing a life point). We also experiment with a modified version of the reward function, where agents are given +0.01 for surviving a timestep.
#
# Thus, we have the following characterization of our environment:
# - **Episodic**: After any of the agents loses 5 lives an episode terminates. Stalemates are possible, so we limit an episode to 3000 steps.
# - **Stochastic**: Given a state and an action, we don't necessarily know the next state, since it depends on the opponent's actions.
# - **Observation space**: The observation space is a continuous 12-D vector with features for the position and velocity of the agent (`ag`), the opponent (`op`), and the ball (`ball`):
#   $\mathcal{S} = (x_\text{ag}, y_\text{ag}, \dot{x}_\text{ag}, \dot{y}_\text{ag}, x_\text{ball}, y_\text{ball}, \dot{x}_\text{ball}, \dot{y}_\text{ball}, x_\text{op}, y_\text{op}, \dot{x}_\text{op}, \dot{y}_\text{op})$
# - **Action space**: The action space is a set of 6 discrete movement directions the agent can take: $\mathcal{A} = \{\text{STAND}, \text{LEFT}, \text{RIGHT}, \text{UP}, \text{UPRIGHT}, \text{UPLEFT}\}$
#
# Results are compared against a random policy which achieves a mean score of $-4.866 \pm 0.372$ against the baseline agent over a total of 512 matches.

# %% ######################################################################### [markdown]
# # DQN (Deep Q-Network)
# ## Introduction
# Deep Q-Networks (DQN) were first introduced in 2013 by DeepMind as the
# first Deep Reinforcement Learning (DRL) model to learn control policies directly from
# high-dimensional sensory input.  The same model, with no adjustments to architecture or
# algorithm, learned to play 7 different Atari 2600 games.
#
# DQN uses experience replay and fixed Q-targets to stabilize learning. Experience
# replay allows for greater data efficiency, since each step of experience is potentially
# used in many weight updates. Learning from consecutive samples is inefficient due to
# strong correlation between samples. By randomizing the experience sampling, we break the
# correlation and reduces variance of updates.
#
# Fixing the Q-target is also important, since there is also lot of correlation between
# Q-values and Q-targets. If we were to update the Q-target while trying to
# approximate it, the neural network could spiral out of control and fail to converge.
#
# ## How it works
# First, we take an action $a_t$ with $\epsilon$-greedy policy (_off-policy_).
# Then, we store transitions $(s, a, r, s')$ in a replay memory
# $\mathcal{D}$. When training, we sample a random mini-batch $\mathcal{B}$ of transitions
# from $\mathcal{D}$ and compute our Q-learning values and targets. The targets are
# computed with respect to old, fixed parameters $\theta^-$. We optimize the MSE between
# Q-network predictions and the Q-learning targets. We wish to optimize the cost function
# below:
#
# $$\mathcal{L}(\theta) = \mathbb{E}_{s,a,r,s'\sim\mathcal{D}} \left[ \left(r +  \gamma
# \max_{a'} Q^{\theta^-}(s', a') - Q^{\theta}(s, a)\right)^2 \right]$$.
#
# ## Experiments
# For the DQN experiments, we used the following parameters:
# - Q-network is a Multi-Layer Perceptron (MLP) with two hidden layers, each with size
# $64$;
# - Learning rate linearly decays from $0.0001$ to $0$ throughout training;
# - Discount factor set to $\gamma = 0.99$;
# - Training stops when mean evaluation return is $\ge 0$ or $t > 5\times10^6$, where $t$
# is the number of training steps;
# - Learning starts after collecting $5 \times 10^4$ experiences;
# - Target network is updated every $t_\text{update}$ steps;
# - Gradients are clipped to $10$;
# - Replay buffer has size $|\mathcal{D}| = 10^6$, from where we sample $|\mathcal{B}|$
# experiences uniformly every update;
# - Exploration decays for fraction $\epsilon_f$ of training from $1.0$ to $0.05$.
#
# We experimented varying the batch size $|\mathcal{B}|$, tuning the fraction of training
# we spent exploring $\epsilon_f$, and the interval between updates to the target network
# $t_\text{update}$. We varied the parameters in this order and selected the best one for
# each experiment to use on the next one.
#
# ## Results
#
# The table below shows the results from all experiments we ran with DQN. The next
# section details the insights we can take from varying each parameter.
#
# | $\mathcal{B}$ | $\epsilon_f$ | $t_\text{update}$  | $t_\text{end}$ | $\mathbb{E}[\tau]$ | $\mathbb{E}[G]$  |
# |:--------------:|:------------:|:-----------------:|:--------------:|:------------------:|:----------------:|
# |      32        |     0.1      |  $10^4$           |  $5 \times 10^6$  |   $2393 \pm 645$   | $-4.00 \pm 1.22$ |
# |      64        |     0.1      |  $10^4$           |  $5 \times 10^6$  |   $2941 \pm 257$   | $-1.50 \pm 1.77$ |
# |      128       |     0.1      |  $10^4$           |  $5 \times 10^6$  |   $1424 \pm 479$   | $-4.65 \pm 0.91$ |
# |      64        |     0.2      |  $10^4$           |  $5 \times 10^6$  |    $3000 \pm 0$    | $-0.65 \pm 0.65$ |
# |      64        |     0.5      |  $10^4$           |  $5 \times 10^6$  |   $2961 \pm 167$   | $-1.50 \pm 1.63$ |
# |      64        |     0.2      |  $5 \times 10^3$  |  $4.5 \times 10^6$  |    $3000 \pm 0$    | $0.10 \pm 0.94$  |
# |      64        |     0.2      |  $2 \times 10^4$  |  $5 \times 10^6$  |    $3000 \pm 0$    | $-0.25 \pm 1.30$ |
#
#
# ### Batch size ($|\mathcal{B}| \in \{32, 64, 128\}$)
# For the first experiment, we varied the batch size $|\mathcal{B}|$ to see if increasing
# it would help stabilize learning. For $|\mathcal{B}| = 32$ learning was much slower. This
# is expected considering a smaller batch size decreases the number of times experiences
# are replayed and has subject to more variance in the updates. We expected that
# further increasing the batch size would contribute to the stability of the algorithm
# and possibly improve the convergence time. However, from the graphs below, we can
# see how increasing $|\mathcal{B}|$ past $64$, doesn't contribute to the stability and
# both algorithms behave converge similarly. For future experiments, we decided
# to stick with $|\mathcal{B}| = 64$.
#
# <img src="https://imgur.com/kK2VQBr.png" alt="drawing" width="700"/>
#
# ### Exploration fraction ($\epsilon_f \in \{0.1, 0.2, 0.5\}$)
# For the second experiment, we varied the fraction $\epsilon_f$ of training we
# spent decaying $\epsilon$ from 1.0 to 0.05. We found that using an
# intermediate value for $\epsilon_f$, was best. Exploring too little led the
# agent to sample an insufficient variety of experiences to converge to a good
# local optimum. Exploring too much didn't give the agent enough time to finish
# exploiting its experiences.  It's hard to say if using $\epsilon_f = 0.5$ is
# objectively worse or if the agent just didn't have enough time to exploit its
# learned experience.
#
# <img src="https://imgur.com/8xODv9E.png" alt="drawing" width="700"/>
#
# ### Q-target updates ($t_\text{update} \in \{5000, 10000, 20000\}$)
# For the third experiment, we varied the number of steps $t_\text{update}$
# between updates to the Q-target network $Q^{\theta-}$. Notice how shorter
# update intervals benefitted the algorithm and let a faster convergence. We
# suspect that using too large of an update interval leads the network to
# approximate its previously poor estimate of $\theta^-$ for too long, which can
# make training slower.
#
# <img src="https://imgur.com/M42MJNE.png" alt="drawing" width="700"/>

# %% ######################################################################### [markdown]
# # PPO (Proximal Policy Optimization)
# ## Introduction
# Introduced by OpenAI in late 2017 as an alternative to other policy gradient algorithms, which is simpler to implement and fine-tune, PPO achieved state-of-the-art results (comparable to TRPO and ACER).
#
# It aims not to change the policy network too drastically after an update, which can lead to unrecoverable performance collapse (similar idea to TRPO, _Trust Region Policy Optimization_).
#
# Unlike DQN which learns from stored _offline_ data, PPO is a policy-gradient method, thus, it is capable of learning _online_ (as the agent experiences the environment).
#
# ## How it works
# By clipping (i.e. truncating) the value of the objective function, where $\hat{A} = A^{\pi_{\theta_{\text{old}}}}(s,a)$ is the advantage function and the probability ratio $\mathrm{r}(\theta) = \frac{\pi_{\theta}(a|s)}{\pi_{\theta_{\text{old}}}(a|s)}$:
# $$\mathcal{L}(\theta) = \mathbb{E}_{s,a\sim\pi_{\theta_{\text{old}}}}[\text{min}\{\mathrm{r}(\theta)\hat{A}, \text{clip}(\mathrm{r}(\theta), 1-\epsilon, 1+\epsilon)\hat{A}\}]$$,
# PPO acts as a regularizer, removing incentives for policy updates to deviate too much from the outputs of the old version of the policy network.
#
# We can see a comparison of the clipping effect when the advantage is positive or negative: $\text{clip}(\mathrm{r}(\theta), 1-\epsilon, 1+\epsilon)\hat{A}$, with $\epsilon$ usually varying from 0.1 to 0.3: ![](https://i.imgur.com/nJoxCZV.png)
#
# If taking a certain action became more probable ($\mathrm{r}(\theta) > 1$) after the last update, and the advantage is positive, we limit its update since, as we are dealing with neural networks, a value too large could be the result of noise. In that same way, if an action just became a lot less probable ($\mathrm{r}(\theta) < 1$) and the advantage is negative, we retain from drastically reducing it's likelihood.
#
# ## Experiments
# For the PPO experiments, we fixed the following parameters:
# - Both policy and value function networks (_Actor_ and _Critic_) are MLPs with two hidden layers, each with size $64$;
# - Learning rate linearly decays from $0.0003$ to $0$ throughout training;
# - Discount factor and training stop criteria were kept the same as DQN;
# - Advantage values are normalized (i.e. $\hat{A} \leftarrow \frac{\hat{A} - \mu}{\sigma}$);
# - _Critic_ updates the value function parameters every $n_\text{steps}$ steps;
# - No clipping is done to the value function;
# - There was no early stopping when limiting the estimated KL divergence between updates to $\{0.01, 0.03, 0.05\}$;
# - $\epsilon$ varied from $\{0.1, 0.2, 0.3\}$.
#
#
# We experimented varying the policy clip range ($\epsilon$ value) and the number of steps $n_\text{steps}$ to run for each environment per (_Critic_) update.
#
# We also tested whether or not setting a target limit $T_{KL} = \{0.01, 0.02, $ $0.03\}$ to the KL divergence between (_Actor_) updates to the policy network would lead training to stop earlier. However, since it didn't do so in the preliminary tests, we proceeded without it.
#
# ## Results
#
# | $\epsilon_f$ | $n_\text{steps}$ | $t_\text{end}$ | $\mathbb{E}[\tau]$ | $\mathbb{E}[G]$  |
# |:------------:|:----------------:|:--------------:|:------------------:|:----------------:|
# |     0.1      |      $2048$      |  $3.5 \times 10^6$  |   $3000 \pm 0$ | $0.30 \pm 1.14$ |
# |     0.2      |      $2048$      |  $3.5 \times 10^6$  |    $3000 \pm 0$ | $0.05 \pm 0.80$ |
# |     0.3      |      $2048$      |  $4.5 \times 10^6$  |   $678 \pm 128$ | $-4.85 \pm 0.36$ |
# |     0.2      |      $1024$      |  $4.5 \times 10^6$  |    $571 \pm 89$ | $-4.90 \pm 0.30$ |
# |     0.2      |      $4096$      |  $3 \times 10^6$  |    $3000 \pm 0$ | $0.05 \pm 0.74$ |
#
#
# ### Clip range size ($\epsilon \in \{0.1, 0.2, 0.3\}$)
# For the first experiment, we varied the $\epsilon$ value, which controls how much clipping is done to the policy objective function.
#
# We observe that greater values give more leeway to increasing the policy update, but this may lead to unrecoverable extremes, as was the case with $\epsilon = 0.3$ (the single value which didn't converge).
#
# <img src="https://imgur.com/oyrEgeW.png" alt="drawing" width="700"/>
#
# ### Number of steps ($n_{steps} \in \{1024, 2048, 4096\}$)
# For the second experiment, we varied the number $n_{steps}$ of experiences that are collected before each update to the network parameters.
#
# Note that, when the agent gets close to 0 mean return, episodes terminate by the 3000 steps limit criteria.
# Hence, using $n_{steps} = 1024$ might not carry enough information, while 2048 and 4096 get more complete episodes.
#
# <img src="https://imgur.com/aKlnTPB.png" alt="drawing" width="700"/>

# %% ######################################################################### [markdown]
# # Reward Shaping
# In this experiment, we examined the effect of using $R_\text{sb}$, which changes
# $R_\text{sc}$ by adding a survival reward bonus of +0.01 per time step.  For each
# algorithm, we used the best configurations from previous experiments.  Instead of
# evaluating the mean return over 20 epochs during training, we looked at the final mean
# return over 1000 epochs. The table below shows the results we obtained for the four
# experiments, two for each algorithm:
#
# | Algorithm | Reward        | $\mathbb{E}[G]$    |
# |:---------:|:-------------:|:------------------:|
# | DQN       | $R_\text{sc}$ | $-0.438 \pm 1.092$ |
# | DQN       | $R_\text{sb}$ | $-0.008 \pm 1.059$ |
# | PPO       | $R_\text{sc}$ | $-0.068 \pm 0.830$ |
# | PPO       | $R_\text{sb}$ | $0.121 \pm 0.878$  |
#
# Notice how both algorithms improved with the change in the reward function.  Since we're
# playing against an "expert" baseline agent, it's hard to get the initial positive
# rewards which start directing us towards a good policy.  By adding a survival bonus, the
# agent can learn that bouncing back the ball to the opponent is good, and adjust its
# weights earlier in a direction that does so more often.

# %% ######################################################################### [markdown]
# # Conclusion
# Policy gradient methods typically converge faster to local optima.  They model
# probabilities of actions, so they're capable of learning stochastic policies, while DQN
# isn't. Policy gradient methods also typically have a higher variance and are less sample
# efficient than value iteration ones.  However, due to clipping, PPO can mitigate the
# high variance effect, while benefiting from faster convergence.  In our experiments, we
# didn't investiagte the local optima convergence effect of policy gradient methods, since
# we stopped training as soon as the agent beat the baseline opponent. With enough
# training time, it would be possible to see if DQN converges to better optima.
#
# In our experiments, we observed that PPO was considerably faster in converging to our
# local optima of matching the expert baseline.  PPO converged as early as 3M steps, while
# DQN's best configuration took 4.5M.  PPO was also much more stable across a variety of
# different parameters, while DQN was more sensitive and often didn't converge.
# By using continuous rewards improved convergence speed even further and netted a higher
# mean return for both algorithms.

# %% ######################################################################### [markdown]
# # Future Work
# In the future, we would like to experiment with self-play, since training against a
# baseline means our agent will be biased towards beating its opponent and has a harder
# time to start learning.
#
# We would also like to experiment learning from high-dimensional
# sensory input, such as the raw image pixels of the environment, using CNNs instead of
# MLPs. One interesting test would be to freeze one of our best MLP network parameters
# and plug a small convolutional network in front of it (backpropagating only on it),
# and seeing whether or not this could get good results faster than a CNN learning
# completely from scratch.
#
# ![](https://camo.githubusercontent.com/5833cd0438191b0a1b5f682a1b17820eef45bcf86ff11c9eccc4ef2b94917729/68747470733a2f2f6d656469612e67697068792e636f6d2f6d656469612f57334e4974563650494e6d626755464b50662f67697068792e676966)

# %% ######################################################################### [markdown]
# # Member contributions (Group 8)
# - Christian Maekawa (231867): -
# - Felipe Tavares (265680): -
# - Luiz Eduardo Cartolano (183012): Ran experiments for PPO and worked on the
# slides and did the video presentation.
# - Rafael Figueiredo Prudencio (186145): Trained initial converging configs for each
# algorithm with stable-baselines. Implemented and ran experiments for DQN. Implemented
# visualizations, helped with video presentation and writing the notebook.
# - Tiago Chaves (187690): First tests with stable-baselines3, PPO implementation and
# experiments, video presentation and jupyter notebook.
