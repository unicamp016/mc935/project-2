"""Train baseline DQN using stable-baselines3."""
import os

import fire
import gym
import slimevolleygym
from stable_baselines3.common import logger
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.dqn import DQN, MlpPolicy

from src.callbacks import get_default_callback_list
from src.consts import SEED, TB_LOG_DIR, TOTAL_TIMESTEPS
from src.utils import get_env_name, get_log_dir, gym_env, linear_schedule, save_config


@save_config
def dqn(name,
        learning_rate=1e-4,
        buffer_size=int(1e6),
        train_freq=4,
        target_update_interval=1e4,
        learning_starts=5e4,
        exploration_fraction=1e-1,
        batch_size=64,
        survival_bonus=False):
    log_dir = get_log_dir(name)
    with gym_env(get_env_name(discrete_act_space=True), survival_bonus, seed=SEED) as env:
        logger.configure(folder=log_dir)
        env = Monitor(env, log_dir)
        model = DQN(
            MlpPolicy,
            env,
            buffer_size=buffer_size,
            learning_starts=learning_starts,
            target_update_interval=target_update_interval,
            train_freq=train_freq,
            exploration_fraction=exploration_fraction,
            batch_size=batch_size,
            learning_rate=linear_schedule(learning_rate),
            tensorboard_log=TB_LOG_DIR,
            verbose=2,
            seed=SEED,
        )
        callbacks = get_default_callback_list(env, log_dir)
        model.learn(total_timesteps=TOTAL_TIMESTEPS, callback=callbacks, tb_log_name=name)
        model.save(os.path.join(log_dir, "final_model_dqn"))


if __name__ == "__main__":
    fire.Fire(dqn)
