"""Train baseline PPO using stable-baselines3."""
import os

import fire
import gym
import slimevolleygym
from stable_baselines3.common import logger
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.ppo import PPO, MlpPolicy

from src.callbacks import get_default_callback_list
from src.consts import SEED, TB_LOG_DIR, TOTAL_TIMESTEPS
from src.utils import get_env_name, get_log_dir, gym_env, linear_schedule, save_config


@save_config
def ppo(
    name,
    learning_rate=3e-4,
    batch_size=64,
    survival_bonus=False,
    discrete_act_space=False,
    # NOTE using the same defaults as SB3's PPO:
    clip_range=0.2,
    clip_range_vf=None,
    target_kl=None,
    n_steps=2048,
):
    log_dir = get_log_dir(name)
    with gym_env(get_env_name(discrete_act_space), survival_bonus, seed=SEED) as env:
        logger.configure(folder=log_dir)
        env = Monitor(env, log_dir)
        model = PPO(
            MlpPolicy,
            env,
            batch_size=batch_size,
            learning_rate=linear_schedule(learning_rate),
            tensorboard_log=TB_LOG_DIR,
            verbose=2,
            seed=SEED,
            clip_range=clip_range,
            clip_range_vf=clip_range_vf,
            target_kl=target_kl,
            n_steps=n_steps,
        )
        callbacks = get_default_callback_list(env, log_dir)
        model.learn(total_timesteps=TOTAL_TIMESTEPS, callback=callbacks, tb_log_name=name)
        model.save(os.path.join(log_dir, "final_model_ppo"))


if __name__ == "__main__":
    fire.Fire(ppo)
