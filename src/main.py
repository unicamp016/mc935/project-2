import argparse

from src.logger import log_debug


def main():
    log_debug("Hello world!")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    main(**vars(args))
