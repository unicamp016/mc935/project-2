import glob
import inspect
import os
from collections import defaultdict
from contextlib import contextmanager

import gym
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import yaml
from stable_baselines3.common.env_checker import check_env
from stable_baselines3.common.utils import set_random_seed
from tensorboard.backend.event_processing.event_accumulator import EventAccumulator

import src.slimevolley
from src.consts import DISCRETE_ENV_NAME, LOG_DIR, METRIC_TO_TF_TAG, MULTIBIN_ENV_NAME, TB_LOG_DIR
from src.logger import log_error


def set_seed(seed):
    """Set random seed for random, numpy, torch, and tensorflow packages."""
    set_random_seed(seed)
    tf.random.set_seed(seed)


def get_log_dir(name):
    return os.path.join(LOG_DIR, name)


def get_env_name(discrete_act_space):
    return DISCRETE_ENV_NAME if discrete_act_space else MULTIBIN_ENV_NAME


@contextmanager
def plt_fig(save, *args, **kwargs):
    """Context manager to handle matplotlib figures."""
    fig, axes = plt.subplots(*args, **kwargs)
    try:
        yield fig, axes
        if save is None:
            fig.waitforbuttonpress()
        else:
            fig.savefig(save)
    finally:
        plt.close(fig)


@contextmanager
def gym_env(env_name, survival_bonus=False, seed=None, **kwargs):
    """Context manager for gym environment."""
    set_seed(seed)
    env = gym.make(env_name, **kwargs)
    env.survival_bonus = survival_bonus  # NOTE adds +0.01 to reward from step
    env.multiagent = False  # NOTE removes otherObs from info (might be faster)
    check_env(env)
    try:
        yield env
    finally:
        env.close()


def synthesize(array):
    return {
        "mean": np.mean(array),
        "std": np.std(array),
        "min": np.amin(array),
        "max": np.amax(array),
    }


def bgr_to_rgb(bgr, non_contiguous_view=False):
    if non_contiguous_view:
        return bgr[..., ::-1]
    else:
        return bgr[..., ::-1].copy()


def linear_schedule(learning_rate):
    """Return a function that linearly decreases the learning rate
    with the currently remaining progress (which goes from 1 to 0).
    """
    return lambda progress: learning_rate * progress


def get_default_kwargs(func):
    """Return the default keyword arguments for `func`."""
    signature = inspect.signature(func)
    def_kwargs = {"func_name": func.__name__}
    for name, param in signature.parameters.items():
        if not param.default is param.empty:
            def_kwargs[name] = param.default
    return def_kwargs


def save_config(func):
    """Save experiment configuration."""

    def exp_wrapper(name, overwrite=False, **kwargs):
        log_dir = get_log_dir(name)
        if not overwrite and os.path.isdir(log_dir):
            log_error("Experiment directory already exists. Choose a different name.")
        os.makedirs(log_dir, exist_ok=True)
        with open(os.path.join(log_dir, "config.yaml"), "w") as f:
            def_kwargs = get_default_kwargs(func)
            def_kwargs.update(kwargs)
            yaml.dump(def_kwargs, f)
        func(name, **kwargs)

    return exp_wrapper


def get_tfevents_dir(name):
    return os.path.join(LOG_DIR, TB_LOG_DIR, f"{name}_1")


def tfevents_to_dataframe(name, metrics):
    """Load the tfevents file creating a `EventAccumulator` to load the scalars from
    tensorboard logs.

    Args:
      tfevents_dir: the folder where the tfevents files are
      step: the step type (could be epoch and batch for instance)
    Returns:
      list: return a list of dataframes, each one of them representing one tfevents file
    """
    tfevents_dir = get_tfevents_dir(name)
    event_filename = glob.glob(os.path.join(tfevents_dir, "events.*"))[0]
    ea = EventAccumulator(event_filename).Reload()
    tags = [METRIC_TO_TF_TAG[m] for m in metrics]
    rows = defaultdict(dict)
    for tag, metric in zip(tags, metrics):
        for event in ea.Scalars(tag):
            rows[event.step][metric] = event.value
    df = pd.DataFrame(data=rows.values())
    return df


def set_backend(pgf):
    if pgf:
        matplotlib.use("pgf")
        matplotlib.rcParams.update({
            "pgf.texsystem":
                "pdflatex",
            "font.family":
                "serif",
            "text.usetex":
                True,
            "pgf.rcfonts":
                False,
            "pgf.preamble": (r"\usepackage[utf8x]{inputenc}"
                             r"\usepackage[T1]{fontenc}"
                             r"\usepackage{amsfonts}"
                             r"\usepackage{siunitx}"
                             r"\usepackage{amsmath}")
        })
