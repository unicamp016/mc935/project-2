"""Stable baselines callbacks."""
import numpy as np
from stable_baselines3.common.callbacks import (
    BaseCallback,
    CallbackList,
    EvalCallback,
    StopTrainingOnRewardThreshold,
)

from src.consts import EVAL_EPISODES, EVAL_FREQ, STOP_TRAIN_REWARD_THRESHOLD


def get_default_callback_list(env, log_dir):
    """Return callback with a list of default training callbacks."""
    return CallbackList([
        get_tb_callback(),
        get_eval_callback(env, log_dir),
    ])


def get_eval_callback(env, log_dir):
    """Return a evaluation callback with early stopping based on reward."""
    callback_on_best = StopTrainingOnRewardThreshold(
        reward_threshold=STOP_TRAIN_REWARD_THRESHOLD, verbose=1)
    return EvalCallback(
        env,
        best_model_save_path=log_dir,
        callback_on_new_best=callback_on_best,
        log_path=log_dir,
        eval_freq=EVAL_FREQ,
        n_eval_episodes=EVAL_EPISODES,
    )


def get_tb_callback():
    """Get callback to log tensorboard values."""
    return TensorboardCallback(verbose=1)


class TensorboardCallback(BaseCallback):
    """Custom callback for tracking Monitor data in tensorboard."""

    def __init__(self, verbose=0):
        super().__init__(verbose=verbose)
        self.elapsed_time = 0
        self.n_episodes = 0

    def _on_step(self) -> bool:
        env = self.training_env.envs[0]  # We only handle the case for 1 env.
        done_array = np.array(
            self.locals.get("done") if self.locals.get("done") is not None else self.
            locals.get("dones"))
        done_episodes = np.sum(done_array).item()
        if done_episodes:
            self.logger.record("monitor/episode_length", env.get_episode_lengths()[-1])
            self.logger.record("monitor/episode_reward", env.get_episode_rewards()[-1])
            latest_time = env.get_episode_times()[-1]
            self.logger.record("monitor/episode_time", latest_time - self.elapsed_time)
            self.elapsed_time = latest_time
            self.n_episodes += done_episodes
            self.logger.dump(step=self.n_episodes)

        return True
