"""Atari action SlimeVolley environment."""
from gym.envs import register
from slimevolleygym import SlimeVolleyEnv


class SlimeVolleyAtariEnv(SlimeVolleyEnv):
    """SlimeVolley environment with 12-D observations and Atari actions."""

    from_pixels = False
    atari_mode = True


register(id='SlimeVolleyAtari-v0', entry_point='src.slimevolley:SlimeVolleyAtariEnv')
