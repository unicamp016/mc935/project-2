import os

import fire
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from src.consts import EVAL_FREQ, EXP_NAME_TO_LABEL
from src.utils import plt_fig, set_backend, tfevents_to_dataframe


def plot_metric(experiments, metric_name, param, xlabel, ylabel, save=None):
    with plt_fig(save, figsize=(5, 5)) as (_, ax):
        for exp in experiments:
            df = tfevents_to_dataframe(exp, [metric_name])
            t = np.array(list(df.index))
            if "eval" in metric_name:
                t *= EVAL_FREQ
            ax.plot(t, df[metric_name], label=EXP_NAME_TO_LABEL[exp][param])
        ax.legend()
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)


def plot_eval_reward(experiments, param, train=False, save=None):
    metric_type = "train" if train else "eval"
    metric_name = f"{metric_type}_ep_rew_mean"
    plot_metric(experiments, metric_name, param, "Steps", "$\\mathbb{E}[G]$", save)


def plot_eval_length(experiments, param, train=False, save=None):
    metric_type = "train" if train else "eval"
    metric_name = f"{metric_type}_ep_len_mean"
    plot_metric(experiments, metric_name, param, "Steps", "$\\mathbb{E}[\\tau]$", save)


def plot(*experiments, basename="exp", param="batch_size", pgf=False):
    set_backend(pgf)
    plot_eval_reward(experiments, param, save=f"{basename}-ep-reward.pdf")
    plot_eval_length(experiments, param, save=f"{basename}-ep-length.pdf")


if __name__ == "__main__":
    fire.Fire(plot)
