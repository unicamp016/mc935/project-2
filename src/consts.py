import os
from os.path import abspath, dirname

PROJECT_DIR = dirname(dirname(abspath(__file__)))  # ./src/../
BIN_DIR = os.path.join(PROJECT_DIR, "bin")
LOG_DIR = os.path.join(os.getenv("LOG_DIR_ROOT", PROJECT_DIR), "logs")
TB_LOG_DIR = os.path.join(LOG_DIR, "tb_logs")
os.makedirs(LOG_DIR, exist_ok=True)
os.makedirs(BIN_DIR, exist_ok=True)
os.makedirs(TB_LOG_DIR, exist_ok=True)

IN_WIN32 = os.name == "nt"

SEED = 935
EVAL_FREQ = 500_000
EVAL_EPISODES = 20
TOTAL_TIMESTEPS = 4_500_000
STOP_TRAIN_REWARD_THRESHOLD = 0
DISCRETE_ENV_NAME = "SlimeVolleyAtari-v0"
MULTIBIN_ENV_NAME = "SlimeVolley-v0"

METRIC_TO_TF_TAG = {
    "train_ep_len": "monitor/episode_length",
    "train_ep_rew": "monitor/episode_reward",
    "train_ep_time": "monitor/episode_time",
    "train_exp_rate": "rollout/exploration_rate",
    "train_ep_len_mean": "rollout/ep_len_mean",
    "train_ep_rew_mean": "rollout/ep_rew_mean",
    "train_loss": "train/loss",
    "train_fps": "time/fps",
    "train_lr": "train/learning_rate",
    "eval_ep_len_mean": "eval/mean_ep_length",
    "eval_ep_rew_mean": "eval/mean_reward",
}

# TODO: Infer this from config.yaml
EXP_NAME_TO_LABEL = {
    "dqn_bs_32_llr": {
        "batch_size": "$|\\mathcal{B}| = 32$",
        "exploration": "$\\epsilon_f = 0.1$",
    },
    "dqn_bs_64_llr": {
        "batch_size": "$|\\mathcal{B}| = 64$",
        "exploration": "$\\epsilon_f = 0.1$",
    },
    "dqn_bs_128_llr": {
        "batch_size": "$|\\mathcal{B}| = 128$",
        "exploration": "$\\epsilon_f = 0.1$",
    },
    "dqn_exp_20": {
        "batch_size": "$|\\mathcal{B}| = 64$",
        "exploration": "$\\epsilon_f = 0.2$",
        "update": "$t_\\text{update} = \\num{1e4}$",
    },
    "dqn_exp_50": {
        "batch_size": "$|\\mathcal{B}| = 64$",
        "exploration": "$\\epsilon_f = 0.5$",
    },
    "dqn_updint_5k": {
        "batch_size": "$|\\mathcal{B}| = 64$",
        "exploration": "$\\epsilon_f = 0.2$",
        "update": "$t_\\text{update} = \\num{5e3}$",
    },
    "dqn_updint_20k": {
        "batch_size": "$|\\mathcal{B}| = 64$",
        "exploration": "$\\epsilon_f = 0.2$",
        "update": "$t_\\text{update} = \\num{2e4}$",
    },
    "ppo_clip1": {
        "nsteps": "$n_\\text{steps} = 2048",
        "epsilon": "$\\epsilon = 0.1$",
    },
    "ppo_clip2": {
        "nsteps": "$n_\\text{steps} = 2048",
        "epsilon": "$\\epsilon = 0.2$",
    },
    "ppo_clip3": {
        "nsteps": "$n_\\text{steps} = 2048",
        "epsilon": "$\\epsilon = 0.3$",
    },
    "ppo_nsteps1K": {
        "nsteps": "$n_\\text{steps} = 1024",
        "epsilon": "$\\epsilon = 0.2$",
    },
    "ppo_nsteps4K": {
        "nsteps": "$n_\\text{steps} = 4096",
        "epsilon": "$\\epsilon = 0.2$",
    },
}

if __name__ == "__main__":
    from src.logger import log

    log(f"{IN_WIN32=}")
    log(f"{PROJECT_DIR=}")
    log(f"{BIN_DIR=}")
    log(f"{LOG_DIR=}")
    log(f"{TB_LOG_DIR=}")
